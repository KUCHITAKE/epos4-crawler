﻿// -*- C++ -*-
/*!
 * @file  epos4_crawlerTest.cpp
 * @brief crawler controller using maxon epos4 driver
 * @date $Date$
 *
 * $Id$
 */

#include "epos4_crawlerTest.h"

// Module specification
// <rtc-template block="module_spec">
static const char* epos4_crawler_spec[] =
  {
    "implementation_id", "epos4_crawlerTest",
    "type_name",         "epos4_crawlerTest",
    "description",       "crawler controller using maxon epos4 driver",
    "version",           "1.0.0",
    "vendor",            "NUT KimuraLab",
    "category",          "Controller",
    "activity_type",     "PERIODIC",
    "kind",              "DataFlowComponent",
    "max_instance",      "1",
    "language",          "C++",
    "lang_type",         "compile",
    // Configuration variables
    "conf.default.velocity_limit", "2.0",
    "conf.default.angular_velocity_limit", "2.0",

    // Widget
    "conf.__widget__.velocity_limit", "slider.0.01",
    "conf.__widget__.angular_velocity_limit", "slider.0.01",
    // Constraints
    "conf.__constraints__.velocity_limit", "0.0<x<3.0",
    "conf.__constraints__.angular_velocity_limit", "0.0<x<3.0",

    "conf.__type__.velocity_limit", "double",
    "conf.__type__.angular_velocity_limit", "double",

    ""
  };
// </rtc-template>

/*!
 * @brief constructor
 * @param manager Maneger Object
 */
epos4_crawlerTest::epos4_crawlerTest(RTC::Manager* manager)
    // <rtc-template block="initializer">
  : RTC::DataFlowComponentBase(manager),
    m_velocity_inputIn("velocity_input", m_velocity_input),
    m_actual_velocityOut("actual_velocity", m_actual_velocity)

    // </rtc-template>
{
}

/*!
 * @brief destructor
 */
epos4_crawlerTest::~epos4_crawlerTest()
{
}



RTC::ReturnCode_t epos4_crawlerTest::onInitialize()
{
  // Registration: InPort/OutPort/Service
  // <rtc-template block="registration">
  // Set InPort buffers
  addInPort("actual_velocity", m_actual_velocityIn);

  // Set OutPort buffer
  addOutPort("velocity_input", m_velocity_inputOut);

  // Set service provider to Ports

  // Set service consumers to Ports

  // Set CORBA Service Ports

  // </rtc-template>

  // <rtc-template block="bind_config">
  // Bind variables and configuration variable
  bindParameter("velocity_limit", m_velocity_limit, "2.0");
  bindParameter("angular_velocity_limit", m_angular_velocity_limit, "2.0");
  // </rtc-template>

  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t epos4_crawlerTest::onFinalize()
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t epos4_crawlerTest::onStartup(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t epos4_crawlerTest::onShutdown(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t epos4_crawlerTest::onActivated(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t epos4_crawlerTest::onDeactivated(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t epos4_crawlerTest::onExecute(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t epos4_crawlerTest::onAborting(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t epos4_crawlerTest::onError(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t epos4_crawlerTest::onReset(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t epos4_crawlerTest::onStateUpdate(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t epos4_crawlerTest::onRateChanged(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/



extern "C"
{

  void epos4_crawlerTestInit(RTC::Manager* manager)
  {
    coil::Properties profile(epos4_crawler_spec);
    manager->registerFactory(profile,
                             RTC::Create<epos4_crawlerTest>,
                             RTC::Delete<epos4_crawlerTest>);
  }

};


