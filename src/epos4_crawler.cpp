﻿// -*- C++ -*-
/*!
 * @file  epos4_crawler.cpp
 * @brief crawler controller using maxon epos4 driver
 * @date $Date$
 *
 * $Id$
 */

#include "epos4_crawler.h"
#include <string>

// Module specification
// <rtc-template block="module_spec">
static const char* epos4_crawler_spec[] =
  {
    "implementation_id", "epos4_crawler",
    "type_name",         "epos4_crawler",
    "description",       "crawler controller using maxon epos4 driver",
    "version",           "1.0.0",
    "vendor",            "NUT KimuraLab",
    "category",          "Controller",
    "activity_type",     "PERIODIC",
    "kind",              "DataFlowComponent",
    "max_instance",      "1",
    "language",          "C++",
    "lang_type",         "compile",
    // Configuration variables
    "conf.default.velocity_limit", "2.0",
    "conf.default.angular_velocity_limit", "2.0",

    // Widget
    "conf.__widget__.velocity_limit", "slider.0.01",
    "conf.__widget__.angular_velocity_limit", "slider.0.01",
    // Constraints
    "conf.__constraints__.velocity_limit", "0.0<x<3.0",
    "conf.__constraints__.angular_velocity_limit", "0.0<x<3.0",

    "conf.__type__.velocity_limit", "double",
    "conf.__type__.angular_velocity_limit", "double",

    ""
  };
// </rtc-template>

/*!
 * @brief constructor
 * @param manager Maneger Object
 */
epos4_crawler::epos4_crawler(RTC::Manager* manager)
    // <rtc-template block="initializer">
  : RTC::DataFlowComponentBase(manager),
    m_velocity_inputIn("velocity_input", m_velocity_input),
    m_actual_velocityOut("actual_velocity", m_actual_velocity),
    nodes({EposNode(), EposNode()})

    // </rtc-template>
{
}

/*!
 * @brief destructor
 */
epos4_crawler::~epos4_crawler()
{
}



RTC::ReturnCode_t epos4_crawler::onInitialize()
{
  // Registration: InPort/OutPort/Service
  // <rtc-template block="registration">
  // Set InPort buffers
  addInPort("velocity_input", m_velocity_inputIn);

  // Set OutPort buffer
  addOutPort("actual_velocity", m_actual_velocityOut);

  // Set service provider to Ports

  // Set service consumers to Ports

  // Set CORBA Service Ports

  // </rtc-template>

  // <rtc-template block="bind_config">
  // Bind variables and configuration variable
  bindParameter("velocity_limit", m_velocity_limit, "2.0");
  bindParameter("angular_velocity_limit", m_angular_velocity_limit, "2.0");
  // </rtc-template>

  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t epos4_crawler::onFinalize()
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t epos4_crawler::onStartup(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t epos4_crawler::onShutdown(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t epos4_crawler::onActivated(RTC::UniqueId ec_id)
{
  //open devices

  //device
  std::string deviceName = "EPOS4";
  std::string protocolStackName = "MAXON SERIAL V2";
  std::string interfaceName = "USB";
  std::string portName = "USB0";

  //sub device
  std::string subDeviceName = "EPOS4";
  std::string subProtocolStackName = "CANopen";

  unsigned int errorCode = 0;
  unsigned int baudRate = 0;
  unsigned int timeOut = 0;

  void* keyHandle = 0;
  void* subKeyHandle = 0;

  keyHandle = VCS_OpenDevice((char*)deviceName.c_str(), (char*)protocolStackName.c_str(), (char*)interfaceName.c_str(), (char*)portName.c_str(), &errorCode);
  if(keyHandle > 0) {
    if(VCS_GetProtocolStackSettings(keyHandle, &baudRate, &timeOut, &errorCode)) {
      timeOut += 100;
      VCS_SetProtocolStackSettings(keyHandle, baudRate, timeOut, &errorCode);
    }

    subKeyHandle = VCS_OpenSubDevice(keyHandle, (char*)subDeviceName.c_str(), (char*)subProtocolStackName.c_str(), &errorCode);
    if(subKeyHandle > 0) {
      std::cout << "devices opened successfully" << std::endl;
      if(VCS_GetGatewaySettings(keyHandle, &baudRate, &errorCode)) {
        std::cout << "baudRate: " << baudRate << std::endl;
      }
    } else {
      std::cout << "failed to open sub device" << std::endl;
      return RTC::RTC_ERROR;
    }
  } else {
    std::cout << "failed to open device" << std::endl;
    return RTC::RTC_ERROR;
  }


  //prepare devices
  nodes = {EposNode(keyHandle, false, LEFT_NODE_ID), EposNode(subKeyHandle, true, RIGHT_NODE_ID),};
  for(auto& node: nodes) {
    auto& key = node.keyHandle;
    auto& id = node.id;
    unsigned int errorCode = 0;

    int isFault;
    VCS_GetFaultState(key, id, &isFault, &errorCode);
    if(isFault) {
      std::cout << "clear fault, node = " << id << std::endl;
      VCS_ClearFault(key, id, &errorCode);
    }

    int isEnabled;
    VCS_GetEnableState(key, id, &isEnabled, &errorCode);
    if(!isEnabled) {
      VCS_SetEnableState(key, id, &errorCode);
    }

    VCS_ActivateProfileVelocityMode(key, id, &errorCode);
  }

  return RTC::RTC_OK;
}


RTC::ReturnCode_t epos4_crawler::onDeactivated(RTC::UniqueId ec_id)
{
  unsigned int errorCode =0;
  for(auto& node: nodes) {
    VCS_HaltVelocityMovement(node.keyHandle, node.id, &errorCode);

    if(node.isSubDevice) {
      VCS_CloseSubDevice(node.keyHandle, &errorCode);
    } else {
      VCS_CloseDevice(node.keyHandle, &errorCode);
    }
  }

  return RTC::RTC_OK;
}


RTC::ReturnCode_t epos4_crawler::onExecute(RTC::UniqueId ec_id)
{
  if(m_velocity_inputIn.isNew()) {
    m_velocity_inputIn.read();

    unsigned int errorCode = 0;

    auto& velocityInput = m_velocity_input.data;

    auto leftSpeed = velocityInput.vx * 2000.0 - velocityInput.va * 1000.0;
    auto rightSpeed = velocityInput.vx * 2000.0 + velocityInput.va * 1000.0;
    // std::cout << "leftSpeed: " << leftSpeed << ", rightSpeed: " << rightSpeed << std::endl;

    auto& leftKey = (*std::find_if(nodes.begin(), nodes.end(), [](auto& node){return node.id == LEFT_NODE_ID;})).keyHandle;
    auto& rightKey = (*std::find_if(nodes.begin(), nodes.end(), [](auto& node){return node.id == RIGHT_NODE_ID;})).keyHandle;

    VCS_MoveWithVelocity(leftKey, LEFT_NODE_ID, -leftSpeed, &errorCode);
    VCS_MoveWithVelocity(rightKey, RIGHT_NODE_ID, rightSpeed, &errorCode);
  }

  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t epos4_crawler::onAborting(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t epos4_crawler::onError(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t epos4_crawler::onReset(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t epos4_crawler::onStateUpdate(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t epos4_crawler::onRateChanged(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/



extern "C"
{

  void epos4_crawlerInit(RTC::Manager* manager)
  {
    coil::Properties profile(epos4_crawler_spec);
    manager->registerFactory(profile,
                             RTC::Create<epos4_crawler>,
                             RTC::Delete<epos4_crawler>);
  }

};


